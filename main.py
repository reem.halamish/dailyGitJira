import json
import os

import email_sender
import git_manager
import jira_requests_manager
import template_generator
import date_helper

def get_details():
    with open("details.json") as json_file:
        return json.load(json_file)


def main():
    this_dir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(this_dir)
    details = get_details()
    hours_ago = details["general"]["hours_ago"]

    git_manager.preprocess(**details["git"])
    es = email_sender.EmailSender(**details["gmail"]).start_connection()

    for user in details["users"]:

        try:
            jira_part = jira_requests_manager.get_jira_part(hours_ago=hours_ago, **details["jira"], **user["jira"])
            jira_server_url, jira_hierarchy, jira_total_open_issues = jira_part
        except KeyError:
            jira_server_url, jira_hierarchy, jira_total_open_issues = None, None, None

        git_part = git_manager.folders_to_commits(hours_ago=hours_ago, **details["git"], **user["git"])

        html_message = template_generator.get_html(git_part, jira_server_url, jira_hierarchy, jira_total_open_issues,
                                                   hours_ago, **details["html"], **user["html"])

        es.send_email(subject=date_helper.get_email_subject(hours_ago=hours_ago), message=html_message, **user["gmail"])

    print("DONE. emails sent to " + str(len(details["users"])) + "users 🔥")
    es.close_connection()
    os.chdir(this_dir)


if __name__ == '__main__':
    main()

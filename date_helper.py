import datetime


def get_email_subject(hours_ago=24):
    days_ago = hours_ago // 24
    today = datetime.date.today()
    return "Your work for " + str(days_ago) + " days until " + today.strftime("%A, %d %b %Y")


def get_start_date(hours_ago=24):
    today = datetime.datetime.now()
    start = today - datetime.timedelta(hours=hours_ago)

    return start.strftime("%d/%m/%Y at %I:%M%p")


def get_end_date(hours_ago=24):
    today = datetime.datetime.now()
    return today.strftime("%d/%m/%Y at %I:%M%p")
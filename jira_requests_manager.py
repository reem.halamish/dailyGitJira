import requests

from jira import JiraStatus, JiraIssue, JiraHierarchy

SEARCH_SUFFIX = "/rest/api/2/search"


def clean_server_url(server_url):
    if server_url.endswith(SEARCH_SUFFIX):
        return server_url
    if server_url.endswith("/"):
        server_url = server_url[:-1]
    server_url += SEARCH_SUFFIX
    return server_url


def build_body_request_assigned_to_me(jira_status=JiraStatus.TODO, hours_ago=26):
    return {
        "jql":"statusCategory = %d AND updated >= -%dh AND assignee in (currentUser())"%(jira_status, hours_ago),
        "fields":["id","key", "summary", "status", "subtasks", "parent"]   # remove line to see all fields
        }


def build_body_request_assigned_to_user(username, jira_status=JiraStatus.TODO, hours_ago=26):
    return {
        "jql":"statusCategory = %d AND updated >= -%dh AND assignee = %s"%(jira_status, hours_ago, username),
        "fields":["id","key", "summary", "status", "subtasks", "parent"]   # remove line to see all fields
        }


def build_body_request_my_open_issues(username):
    return {
        "jql":"assignee = %s AND resolution = Unresolved order by updated DESC"%username,
        "fields":["key"]  # just want the total
        }


def get_issues(server_url, hierarchy, jira_status, username, admin_username, admin_password, hours_ago=26):
    server_url = clean_server_url(server_url)
    response = requests.post(
        server_url,
        json=build_body_request_assigned_to_user(
            username=username,
            jira_status=jira_status,
            hours_ago=hours_ago),
        auth=(admin_username, admin_password)
    ).json()
    issues_json = response["issues"]
    for json in issues_json:
        issue = JiraIssue(json['key'], json['fields']['summary'])

        if "parent" in json["fields"]:
            parent_json = json["fields"]["parent"]
            parent = JiraIssue(parent_json['key'], parent_json['fields']['summary'])
            hierarchy.add_issue(jira_status, issue, parent)
        else:
            hierarchy.add_issue(jira_status, issue)


def get_total_issues(server_url, username, admin_username, admin_password):
    server_url = clean_server_url(server_url)
    return requests.post(server_url, json=build_body_request_my_open_issues(username), auth=(admin_username, admin_password)).json()["total"]


def get_jira_part(hours_ago, server_url=None, username=None, admin_username=None, admin_password=None):
    if None in [server_url, username, admin_username, admin_password]:
        raise KeyError("not enough details!")
    server_url = clean_server_url(server_url)
    hierarchy = JiraHierarchy()
    get_issues(server_url, hierarchy, JiraStatus.TODO, username, admin_username, admin_password, hours_ago)
    get_issues(server_url, hierarchy, JiraStatus.WIP, username, admin_username, admin_password, hours_ago)
    get_issues(server_url, hierarchy, JiraStatus.DONE, username, admin_username, admin_password, hours_ago)
    total_open_issues = get_total_issues(server_url, username, admin_username, admin_password)
    return server_url, hierarchy, total_open_issues


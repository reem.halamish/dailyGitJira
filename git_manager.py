import os
import subprocess
import datetime


class Data:
    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, k, kwargs[k])

    def __repr__(self):
        return "Data:\n\t" + "\n\t".join(str(k) + " ---> " + str(v) for k, v in self.__dict__.items())


def cd_dir(directory):
    os.chdir(directory)


def git_pull():
    commands = '''
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
git fetch --all
git pull --all'''
    for command in [c for c in commands.split('\n') if c]:
        os.system(command)


def get_commits_raw_strings(separator_char="~~~", hours_ago=26):
    no_need_to_press_enter = '--no-pager'
    log = 'log'
    since = '--since="' + str(hours_ago) + ' hours ago"'
    reverse = '--reverse'  # so that older commits will aappear first
    all_branches = '--all'

    s = str(separator_char)
    f_email = '%ce'
    f_hash = '%h'
    f_timestamp = '%at'
    f_subject = '%s'
    format_pretty = '--pretty=format:' + f_timestamp + s + f_email + s + f_hash + s + f_subject

    sp = subprocess.run([
        'git',
        no_need_to_press_enter,
        log,
        since,
        all_branches,
        reverse,
        format_pretty
    ], stdout=subprocess.PIPE)
    results = [line for line in sp.stdout.decode().split('\n') if line]

    return results


def parse_commits_to_objects(commit_strings, separator_char="~~~"):
    results = []
    for line in commit_strings:
        timestamp, email, commit_hash, subject = line.split(separator_char)
        results.append(Data(commit_hash=commit_hash,
                            subject=subject,
                            email=email.lower(),
                            time=datetime.datetime.fromtimestamp(int(timestamp)).strftime("%d/%m/%Y at %H:%M")))
    return results


def filter_by_email(commits, email):
    match = email.lower()
    return [c for c in commits if c.email == match]


def get_all_commits_from_folder(folder, email_to_filter=None, hours_ago=26, pull=False):
    cd_dir(folder)
    if pull:
        git_pull()
    commits = parse_commits_to_objects(get_commits_raw_strings(hours_ago=hours_ago))
    if email_to_filter:
        commits = filter_by_email(commits, email_to_filter)
    return commits


def folders_to_commits(folders, email, hours_ago, filter_out_empty_folders=True, **kwargs):
    dictionary = dict()
    for folder in folders:
        dictionary[os.path.basename(folder)] = get_all_commits_from_folder(folder, email, hours_ago)

    if filter_out_empty_folders:
        dictionary = {key: value for key, value in dictionary.items() if value}
    return dictionary


def preprocess(folders, also_pull=True, **kwargs):
    for folder in folders:
        cd_dir(folder)
        if also_pull:
            git_pull()


def run_example():
    for c in get_all_commits_from_folder('/Users/reemhalamish/git_daily/git/Infra', "reem@slatescience.com", 720):
        print(c)

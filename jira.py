class JiraStatus:
    TODO = 2
    DONE = 3
    WIP = 4


class JiraIssue:
    def __init__(self, html_key, summary):
        self.html_key = html_key
        self.summary = summary

    def __repr__(self):
        return "Issue %s" % (self.html_key)

    def __str__(self):
        return "Issue %s" % (self.html_key)

    def __hash__(self):
        return hash(self.html_key)

    def __eq__(self, other):
        return id(self) == id(other) or self.html_key == other.html_key


class JiraHierarchy:
    def __init__(self):
        self.todo = set()
        self.wip = set()
        self.done = set()
        self.issues = dict()
        self.parents = dict()

    def add_issue(self, jira_status, issue, parent_issue=None):
        self.issues[issue.html_key] = issue
        self._get_place(jira_status).add(issue.html_key)
        if parent_issue is not None:
            self.add_parent(issue, parent_issue)

    def add_parent(self, issue, parent_issue):
        self.parents[issue.html_key] = parent_issue

    def _get_place(self, jira_status):
        return {JiraStatus.TODO: self.todo,
                JiraStatus.WIP: self.wip,
                JiraStatus.DONE: self.done}[jira_status]

    def get_from_status(self, jira_status):
        place = self._get_place(jira_status)
        issues = {self.issues[key] for key in place}
        parents_to_issues = dict()
        for issue in issues:
            if issue.html_key not in self.parents:
                continue
            parent = self.parents[issue.html_key]
            if parent not in parents_to_issues:
                parents_to_issues[parent] = set()
            parents_to_issues[parent].add(issue)
        return parents_to_issues

    def amount(self, jira_status):
        return len(self._get_place(jira_status))

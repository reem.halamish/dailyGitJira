import os
import re
import sys
from jira import JiraStatus, JiraHierarchy
import date_helper


def check_no_keywords_left(template, print_template_on_error=True):
    matches = re.findall(r'\${([^}]*)}', template)
    if matches:
        if print_template_on_error:
            print(file=sys.stderr)
            print("~~~~~ Template With Error: ~~~~~", file=sys.stderr)
            print(template, file=sys.stderr)
            print("~~~~~ End Template With Error ~~~~~", file=sys.stderr)
            print(file=sys.stderr)

        raise KeyError("template still have kyes unset: " + str(matches))
        

def generate_from_template(template, keywords):
    for keyword in keywords:
        template = template.replace("${%s}"%(keyword), keywords[keyword])
    check_no_keywords_left(template)
    return template


def read_file(filename):
    abs_path = os.path.join(os.path.dirname(__file__), filename)
    with open(abs_path) as file:
        template = file.read()
    return template


def populate_template(template_filename, keywords=None):
    template_filename = template_filename if template_filename.endswith(".html") else template_filename + ".html"
    return generate_from_template(read_file(template_filename), keywords or dict())


def get_commit_keywords(commit):
    return {
        "commit_hash": commit.commit_hash,
        "summary": commit.subject,
        "email":commit.email,
        "time": commit.time
        }


def process_git_part(folders_to_git_commits):
    if not folders_to_git_commits:
        return populate_template("templates/git_no_commits")
    folders_list = ""
    for folder, git_commits in folders_to_git_commits.items():
        if not git_commits:
            continue
        commits_list = ""
        for commit in git_commits:
            commits_list += populate_template("templates/git_commit_bullet", get_commit_keywords(commit))
        folders_list += populate_template("templates/git_main_bullet", {"header": folder, "commit_bullets": commits_list})
    total_commits = sum(len(commits) for folder, commits in folders_to_git_commits.items())
    return populate_template("templates/git_template", {"total_commits": str(total_commits), "repos_amount": str(len(folders_to_git_commits)), "bullets": folders_list})


def process_jira_status(jira_hierarchy, jira_status):
    parents_to_issues = jira_hierarchy.get_from_status(jira_status)
    text = ""
    for parent, issues in parents_to_issues.items():
        issues_text = "".join(populate_template("templates/jira_one_issue", {"summary": i.summary}) for i in issues)
        text += populate_template("templates/jira_parent_issue_with_children", {"parent_summary": parent.summary, "children": issues_text})
    return text


def clean_server_url_for_template(server_url):
    return server_url[:-1] if server_url.endswith("/") else server_url


def process_jira_part(server_url, jira_hierarchy, jira_total_open_issues, hours_ago):
    if None in [server_url, jira_hierarchy, jira_total_open_issues]:
        return ""

    todo = process_jira_status(jira_hierarchy, JiraStatus.TODO)
    wip = process_jira_status(jira_hierarchy, JiraStatus.WIP)
    done = process_jira_status(jira_hierarchy, JiraStatus.DONE)
    server_url = clean_server_url_for_template(server_url)
    keywords = {"server_url": server_url,
                "total_recently_todo": str(jira_hierarchy.amount(JiraStatus.TODO)),
                "total_recently_wip": str(jira_hierarchy.amount(JiraStatus.WIP)),
                "total_recently_closed": str(jira_hierarchy.amount(JiraStatus.DONE)),
                "total_open": str(jira_total_open_issues),
                "issues_done": done,
                "issues_wip": wip,
                "issues_todo": todo,
                "hours_ago": str(hours_ago),
                "hours_ago_as_days": str(hours_ago // 24)
                }

    return populate_template("templates/jira", keywords)


def get_html(folders_to_git_commits, jira_server_url, jira_hierarchy, jira_total_open_issues, hours_ago, user_pretty_name, contact_developer_email):
    git_html = process_git_part(folders_to_git_commits)
    jira_html = process_jira_part(jira_server_url, jira_hierarchy, jira_total_open_issues, hours_ago)
    keywords = {
        "git_part": git_html,
        "jira_part": jira_html,
        "user_pretty_name": user_pretty_name,
        "contact_developer_email": contact_developer_email,
        "date_start": date_helper.get_start_date(hours_ago=hours_ago),
        "date_end": date_helper.get_end_date(hours_ago=hours_ago)
    }
    return populate_template("templates/main", keywords)



import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 


class EmailSender:
    def __init__(self, sender_address, sender_password, bcc_address=None, **extras):
        self.sender_address = sender_address
        self.sender_password = sender_password
        self.bcc_address = bcc_address

    def start_connection(self):
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        # server.ehlo()  # todo added, supposed to be optional, may can be removed
        server.login(self.sender_address, self.sender_password)
        self.server = server
        return self

    def send_email(self, receiver_address, subject, message):
        multipart_msg = MIMEMultipart()
        multipart_msg['From'] = self.sender_address
        multipart_msg['To'] = receiver_address
        multipart_msg['BCC'] = self.bcc_address
        multipart_msg['Subject'] = subject
        multipart_msg.attach(MIMEText(message, 'html'))

        text = multipart_msg.as_string()
        addresses = [receiver_address, self.bcc_address] if self.bcc_address else [receiver_address]
        self.server.sendmail(self.sender_address, addresses, text)

    def close_connection(self):
        self.server.quit()


# DailyGitJira

DailyGitJira is a project supporting automation in generating a user-activity report including activity in Git and status-change in Jira tickets.

  - Customizable scanning time (default: last 24 hours)
  - supports emailing the report after generation
  - report includes:
    - git commits from all branches, from multiple git repositories
    - jira recently opened\closed\started-working-on tickets

Perfect for project managers, and for people who wants to watch their daily activity (for stadups, personal logging etc)



### Perquisites
  - python 3
  - A dedicated gmail account for sending the mail
     - username
     - password
     - ["allow less secure apps" turned on](https://support.google.com/accounts/answer/6010255?hl=en)
  - git repository (or repositories)
     - configured such that running "git pull" won't require username and\or any prompt. [more details here](https://superuser.com/questions/338511/how-do-i-disable-password-prompts-when-doing-git-push-pull)


### running
run the file ```main.py``` from anywhere you want.


### Development
Want to contribute? Great! pull-requests and comments will be much appreciated.
